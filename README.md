# Godot NONXR Mirror
Tool to create a non xr copy of your Godot XR project (powered by symlinks) to simplify parts of your XR development.

# Setup
* Copy this project next to your XR project
```shell
├── godot-nonxr-mirror/
└── your-xr-project/
```
* Open `godot_nonxr_mirror_setup.tscn`
* Change the "Source Project" path of the root node to your XR project directory
* Run the scene (F6) and push the `Refresh Symlinks` Button.
* Enable your addons (and restart godot)

# How to use
* Create custom test scenes that use the scenes of your xr project 
* Prefer to create new directories to prevent adding test scenes to your XR project
